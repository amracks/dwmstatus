#include <sys/utsname.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc_np.h>

struct utsname *dwm_utsname = NULL;

void populate_uname() {
    dwm_utsname = malloc(sizeof(struct utsname));
    uname(dwm_utsname);
}

struct utsname *get_uname() {
    if (dwm_utsname == NULL) {
        populate_uname();
    }
    return dwm_utsname;
}
