#define _BSD_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/utsname.h>

#include <X11/Xlib.h>

#include "system.h"
#include "mail.h"

#define BATT_ACPI_WH0  "hw.sensors.acpibat0.watthour0"

#define TZ_LA "America/Los_Angeles"
#define TZ_UTC "UTC"
#define TZ_TOKYO "Asia/Tokyo"

#define SSC_MDIR "/usr/home/amarks/Mail/SSC/INBOX"
#define GMAIL_MDIR "/usr/home/amarks/Mail/GMail/INBOX"

char *tzutc = "UTC";

static Display *dpy;

char *
smprintf(char *fmt, ...)
{
	va_list fmtargs;
	char *ret;
	int len;

	va_start(fmtargs, fmt);
	len = vsnprintf(NULL, 0, fmt, fmtargs);
	va_end(fmtargs);

	ret = malloc(++len);
	if (ret == NULL) {
		perror("malloc");
		exit(1);
	}

	va_start(fmtargs, fmt);
	vsnprintf(ret, len, fmt, fmtargs);
	va_end(fmtargs);

	return ret;
}

void
settz(char *tzname)
{
	setenv("TZ", tzname, 1);
}

char *
mktimes(char *fmt, char *tzname)
{
	char buf[129];
	time_t tim;
	struct tm *timtm;

	memset(buf, 0, sizeof(buf));
	settz(tzname);
	tim = time(NULL);
	timtm = localtime(&tim);
	if (timtm == NULL) {
		perror("localtime");
		exit(1);
	}

	if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
		fprintf(stderr, "strftime == 0\n");
		exit(1);
	}

	return smprintf("%s", buf);
}

void
setstatus(char *str)
{
	XStoreName(dpy, DefaultRootWindow(dpy), str);
	XSync(dpy, False);
}

char *
loadavg(void)
{
	double avgs[3];

	if (getloadavg(avgs, 3) < 0) {
		perror("getloadavg");
		exit(1);
	}

	return smprintf("%.2f %.2f %.2f", avgs[0], avgs[1], avgs[2]);
}

int
main(void)
{
	char *status;
	char *avgs;
	char *tmla;
	char *tmutc;
    char *tmtok;
    struct utsname *dwm_utsname = get_uname();
    //double batt_perc;

	if (!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "dwmstatus: cannot open display.\n");
		return 1;
	}

	for (;;sleep(90)) {
		avgs = loadavg();
		tmla = mktimes("%a %b %d %G %H:%M", TZ_LA);
		tmutc = mktimes("%H", TZ_UTC);
        tmtok = mktimes("%H", TZ_TOKYO);
        //batt_perc = batt_remain(1);

		status = smprintf("%s-%s-%s | %s | SSC: %d/%d | GMail: %d/%d | Load:%s | JPN:%s | UTC:%s | %s",
				dwm_utsname->sysname,
				dwm_utsname->machine,
				dwm_utsname->release,
				dwm_utsname->nodename,
                new_mails(SSC_MDIR),
                cur_mails(SSC_MDIR),
                new_mails(GMAIL_MDIR),
                cur_mails(GMAIL_MDIR),
                avgs,
                tmtok,
                tmutc,
                tmla);
                
		setstatus(status);
		free(avgs);
		free(tmla);
		free(tmutc);
		free(status);
	}

	XCloseDisplay(dpy);

	return 0;
}

