#include <sys/param.h>
#include <sys/sysctl.h>
#include <sys/sensors.h>
#include <err.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define SENS_ACPIBAT0 3
#define WATT_SNSR 2
#define WATTHR_LF_CAP 0
#define WATTHR_REM_CAP 3

double batt_remain(unsigned char percent) {
    int mib[5];
    struct sensor wh_sens;
    size_t sens_len = sizeof(struct sensor);
    uint64_t wh_lf,
             wh_rc;

    //MIB for Watt Hours Remaining Capacity
    mib[0] = CTL_HW;
    mib[1] = HW_SENSORS;
    mib[2] = SENS_ACPIBAT0;
    mib[3] = SENSOR_WATTHOUR;
    mib[4] = WATTHR_REM_CAP;;

    if ((sysctl(mib, 5, &wh_sens, &sens_len, NULL, 0)) == -1)
        warn("%s", strerror(errno));
    else
        wh_rc = wh_sens.value;

    //MIB for Watt Hours Last Full Capacity
    mib[4] = WATTHR_LF_CAP;

    if ((sysctl(mib, 5, &wh_sens, &sens_len, NULL, 0)) == -1)
        warn("%s", strerror(errno));
    else
        wh_lf = wh_sens.value;

    return (((double) wh_rc / (double) wh_lf) * 100.0) ;
}
