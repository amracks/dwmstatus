#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <malloc_np.h>

#define MAIL_NEW_DIR "/new"
#define MAIL_CUR_DIR "/cur"

int files_in_dir(char *mdir_path) {
    int file_count = 0;
    DIR *dir_p;
    struct dirent *dir_ent;

    dir_p = opendir(mdir_path);
    while ((dir_ent = readdir(dir_p)) != NULL) {
        if (dir_ent->d_type == DT_REG) {
            file_count++;
        }
    }
    closedir(dir_p);
    return file_count;
}

int new_mails(char *mdir_path) {
    int file_count = 0;
    char *mdir_new_path = malloc(strlen(mdir_path) + 5);
    strcpy(mdir_new_path, mdir_path);
    strcat(mdir_new_path, MAIL_NEW_DIR);
    file_count = files_in_dir(mdir_new_path);
    free(mdir_new_path);
    return file_count;
}

int cur_mails(char *mdir_path) {
    int file_count = 0;
    char *mdir_cur_path = malloc(strlen(mdir_path) + 5);
    strcpy(mdir_cur_path, mdir_path);
    strcat(mdir_cur_path, MAIL_CUR_DIR);
    file_count = files_in_dir(mdir_cur_path);
    free(mdir_cur_path);
    return file_count;
}
